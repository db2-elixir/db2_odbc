defmodule Db2 do

	alias Db2.Query
	alias Db2.Type

	@spec start_link(Keyword.t()) :: {:ok, pid}
	def start_link(opts) do
		DBConnection.start_link(Db2.Protocol, opts)
	end

	@spec query(pid(), binary(), [Type.param()], Keyword.t()) ::
	{:ok, iodata(), Db2.Result.t()}
	def query(conn, statement, params, opts \\ []) do
		DBConnection.prepare_execute(
			conn,
			%Query{name: "", statement: statement},
			params,
			opts
		)
	end

	@spec query!(pid(), binary(), [Type.param()], Keyword.t()) ::
	{iodata(), Db2.Result.t()}
	def query!(conn, statement, params, opts \\ []) do
		DBConnection.prepare_execute!(
			conn,
			%Query{name: "", statement: statement},
			params,
			opts
		)
	end
end


# {:ok, state} = Db2.Protocol.connect [conn_str: "DSN=LGNTST"]

# {:ok, pid} = Db2.start_link [conn_str: "DSN=LGNTST"]

# Db2.query pid, "SELECT UID FROM USER", [], []
