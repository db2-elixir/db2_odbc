defmodule Db2.Error do
	defexception [:message, :odbc_code, constraint_violations: []]

	@type t :: %__MODULE__{
		message: binary(),
		odbc_code: atom() | binary(),
		constraint_violations: Keyword.t()
	}

	def exception(message) do
		%__MODULE__{
		  message: to_string(message)
		}
	end
end
