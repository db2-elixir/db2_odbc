defmodule Db2.Protocol do
	use DBConnection

	alias Db2.ODBC
	alias Db2.Result

	defstruct pid: nil, commit_state: :idle, conn_opts: []
	@type state :: %__MODULE__{
		pid: pid(),
		commit_state: :idle | :transaction | :auto_commit,
		conn_opts: Keyword.t()
	}

	@impl true
	def connect(opts) do
		conn_str = Keyword.get(opts, :conn_str, "DSN=LGNTST") || raise "ODBC connection string is missing"

		conn_opts = opts
		# |> Keyword.put_new(:tuple_row, :off)
		|> Keyword.put_new(:extended_errors, :on)

		# conn_str = [
		#   {"DRIVER", opts[:odbc_driver] || "{Db2}"},
		#   {"SERVER", opts[:hostname] || System.get_env("DB2_HOST") || "localhost"},
		#   {"DATABASE", opts[:database] || System.get_env("DB2_DB")},
		#   {"UID", opts[:username] || System.get_env("DB2_UID")},
		#   {"PWD", opts[:password] || System.get_env("DB2_PWD")}
		# ]
		# |> Enum.map_join(fn {key, value} ->
		#   "#{key}=#{value};" end)
		# |> to_charlist

		case ODBC.start_link(conn_str, conn_opts) do
			{:ok, pid} ->
				{:ok, %__MODULE__{
					pid: pid,
					conn_opts: conn_opts,
					commit_state: if(
						conn_opts[:auto_commit] == :on,
						do: :auto_commit,
						else: :idle
					)
				}}
			response -> response
		end
	end

	@impl true
	def disconnect(_err, state) do
		case ODBC.disconnect(state.pid) do
			:ok -> :ok
			{:error, reason} -> {:error, reason, state}
		end
	end

	@impl true
	def checkin(state) do
		{:ok, state}
	end

	@impl true
	def checkout(state) do
		{:ok, state}
	end

	@impl true
	def ping(state) do
		query = %Db2.Query{name: "ping", statement: "SELECT COUNT(DISTINCT 1) FROM SYSCAT.TABLES"}

		case do_query(query, [], [], state) do
		  {:ok, _, new_state} -> {:ok, new_state}
		  {:error, reason, new_state} -> {:disconnect, reason, new_state}
		  other -> other
		end
	end

	def reconnect(new_opts, state) do
		with :ok <- disconnect("Reconnecting", state), do: connect(new_opts)
	end

	@impl true
	def handle_prepare(query, _opts, state) do
		{:ok, query, state}
	end

	@impl true
	def handle_close(_query, _opts, state) do
		{:ok, nil, state}
	end

	@impl true
	def handle_execute(query, params, opts, state) do
		{status, after_query, message, new_state} = do_query(query, params, opts, state)

		case new_state.commit_state do
		  :idle ->
			with {:ok, _, post_commit_state} <- handle_commit(opts, new_state) do
			  {status, after_query, message, post_commit_state}
			end

		  :transaction ->
			{status, after_query, message, new_state}

		  :auto_commit ->
			with {:ok, post_connect_state} <- switch_auto_commit(:off, new_state) do
			  {status, after_query, message, post_connect_state}
			end
		end
	end

	defp do_query(query, params, opts, state) do
		case ODBC.query(state.pid, query.statement, params, opts) do
			{:error, %Db2.Error{odbc_code: :not_allowed_in_transaction} = reason} ->
				if state.commit_state == :auto_commit do
					{:error, reason, state}
				else
					with {:ok, new_state} <- switch_auto_commit(:on, state),
						do: handle_execute(query, params, opts, new_state)
				end

			{:error, %Db2.Error{odbc_code: :connection_exception} = reason} ->
				{:disconnect, reason, state}

			{:error, reason} ->
				{:error, reason, state}

			{:selected, columns, rows} ->
				if query.name == "ping" do
					{:ok,
						%Result{
							columns: Enum.map(columns, &to_string(&1)),
							rows: rows,
							num_rows: Enum.count(rows)
					}, state}
				else
					{:ok,
						query,
						%Result{
							columns: Enum.map(columns, &to_string(&1)),
							rows: rows,
							num_rows: Enum.count(rows)
					}, state}
				end

			{:updated, num_rows} ->
				{:ok, %Result{num_rows: num_rows}, state}
		end
	end

	defp switch_auto_commit(new_value, state) do
		reconnect(Keyword.put(state.conn_opts, :auto_commit, new_value), state)
	end

	@impl true
	def handle_begin(opts, state) do
		case Keyword.get(opts, :mode, :transaction) do
			:transaction -> handle_transaction(:begin, opts, state)
			:savepoint -> handle_savepoint(:begin, opts, state)
		end
	end

	@impl true
	def handle_commit(opts, state) do
		case Keyword.get(opts, :mode, :transaction) do
			:transaction -> handle_transaction(:commit, opts, state)
			:savepoint -> handle_savepoint(:commit, opts, state)
		end
	end

	@impl true
	def handle_rollback(opts, state) do
		case Keyword.get(opts, :mode, :transaction) do
			:transaction -> handle_transaction(:rollback, opts, state)
			:savepoint -> handle_savepoint(:rollback, opts, state)
		end
	end

	defp handle_transaction(:begin, _opts, state) do
		case state.commit_state do
			:idle -> {:ok, %Result{num_rows: 0}, %{state|commit_state: :transaction}}
			:transaction -> {:error, %Db2.Error{message: "Already in transaction"}, state}
			:auto_commit -> {:error, %Db2.Error{message: "Transactions not allowed in autocommit mode"}, state}
		end
	end

	defp handle_transaction(:commit, _opts, state) do
		case ODBC.commit(state.pid) do
			:ok -> {:ok, %Result{}, %{state|commit_state: :idle}}
			{:error, reason} -> {:error, reason, state}
		end
	end

	defp handle_transaction(:rollback, _opts, state) do
		case ODBC.rollback(state.pid) do
			:ok -> {:ok, %Result{}, %{state | commit_state: :idle}}
			{:error, reason} -> {:disconnect, reason, state}
		end
	end

	defp handle_savepoint(:begin, opts, state) do
		if state.commit_state == :auto_commit do
			{:error, %Db2.Error{message: "Savepoints not allowed in autocommit mode"}, state}
		else
			handle_execute(%Db2.Query{name: "", statement: "SAVEPOINT db2_ex_savepoint"}, [], opts, state)
		end
	end


	defp handle_savepoint(:commit, _opts, state) do
		{:ok, %Result{}, state}
	end

	defp handle_savepoint(:rollback, opts, state) do
		handle_execute(%Db2.Query{name: "", statement: "ROLLBACK TO SAVEPOINT db2_ex_savepoint"}, [], opts, state)
	end

	@impl true
	def handle_status(_opts, state) do
		state.commit_state
	end

end
