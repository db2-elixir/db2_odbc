defmodule Db2.ODBC do
    use GenServer

    def start_link(conn_str, opts) do
        GenServer.start_link(__MODULE__, [{:conn_str, to_charlist(conn_str)} | opts])
    end

    def stop(pid) do
        GenServer.stop(pid, :normal)
    end

    def query(pid, statement, params, opts) do
        if Process.alive?(pid) do
            GenServer.call(
              pid,
              {:query, %{statement: IO.iodata_to_binary(statement), params: params}},
              Keyword.get(opts, :timeout, 5000)
            )
          else
            {:error, %Db2.Error{message: :no_connection}}
      end
    end

    def commit(pid) do
        if Process.alive?(pid) do
            GenServer.call(pid, :commit)
        else
            {:error, %Db2.Error{message: :no_connection}}
        end
    end

    def rollback(pid) do
        if Process.alive?(pid) do
          GenServer.call(pid, :rollback)
        else
          {:error, %Db2.Error{message: :no_connection}}
        end
    end

    def disconnect(pid) do
        rollback(pid)
        GenServer.stop(pid, :normal)
    end

    # Callbacks

    def init(opts) do
        connect_opts =
            opts
            |> Keyword.delete_first(:conn_str)
            |> Keyword.put_new(:auto_commit, :off)
            |> Keyword.put_new(:timeout, 5000)
            |> Keyword.put_new(:extended_errors, :on)
            # |> Keyword.put_new(:tuple_row, :off)
            |> Keyword.put_new(:binary_strings, :on)

        case handle_errors(:odbc.connect(opts[:conn_str], connect_opts)) do
            {:ok, odbc_pid} -> {:ok, odbc_pid}
            {:error, reason} -> {:stop, reason} # init should return :stop on errors
        end
    end

    def terminate(_, odbc_pid) do
        :odbc.disconnect(odbc_pid)
    end

    def handle_call({:query, %{statement: statement, params: params}}, _, odbc_pid) do
        {:reply, handle_errors(:odbc.param_query(odbc_pid, to_charlist(statement), params)), odbc_pid}
    end

    def handle_call(:commit, _, odbc_pid) do
        {:reply, handle_errors(:odbc.commit(odbc_pid, :commit)), odbc_pid}
    end

    def handle_call(:rollback, _, odbc_pid) do
        {:reply, handle_errors(:odbc.commit(odbc_pid, :rollback)), odbc_pid}
    end

    defp handle_errors({:error, reason}), do: {:error, Db2.Error.exception(reason)}
    defp handle_errors(term), do: term
end
